# Propuesta solución Laboratorio 2:

- Creado por: Benjamín Rojas Rojas.
- Docente: Alejandro Valdés Jiménez.
- Ayudante: Claudio Guevara.
- Módulo: Sistemas Operativos y Redes.


## Acerca del programa:

- Consiste en un programa que realiza la descarga del audio de un video en formato
mp3 y, posteriormente, lo reproduce mediante la terminal. Esto gracias a la utilización
de "youtube-dl" para descargar, en primer lugar, el audio del video en formato mp3;
y "mplayer" para la reproducción del audio mediante la terminal.

## Funcionamiento e Interacción:

- El programa se inicializa ingresando un parámetro, el cual corresponde al link
del video del cual queremos extraer su audio, luego de ingresar el comando "./Programa"
en la terminal, con el cual se ejecuta el mismo. Un ejemplo es: "./Programa link_vidio_balong".
De este modo se le indica al programa que se debe descargar el audio en formato
mp3 del link dado. Por terminal se indica el link del audio que se descargará y
se procede a descargar el mismo; posterior a esto, se le menciona al usuario que
se reproducirá el audio que ha descargado.
Cabe mencionar que si no se ingresa ningún parámetro o más de lo que se requiere
(1 link), el programa indicará que existe un error, dará un ejemplo de como se debe
ingresar el parámetro y se cerrará.


## Fallos que posee el programa:

- Como se cambia el nombre del archivo descargado, una vez que se quiere descargar
otra canción para reproducirla del mismo modo, esto no ocurre, puesto que se reproduce
el mismo archivo que se ha descargado la primera vez. Por lo tanto, para reproducir
otra canción se debe cambiar el nombre de "audio_vidio.mp3" o, bien, borrar el primer
archivo descargado.


## Construcción del programa:

- Sistema operativo: Ubuntu 18.04.4 LTS.
- Atom: Editor de texto utilizado para escribir el código del programa.
- C++: Lenguaje utilizado para resolver la problemática.


## Puntos a considerar:

- Se utilizan clases.
- Se utiliza fork para descargar correctamente el audio gracias a la llamada de
yotube-dl.
- Se debe realizar la instalación de "make" para que no ocurra ningún problema
en la compilación, esto con "sudo apt install make".
- Se debe realizar la instalación de "youtube-dl" para poder descargar videos o sus
audios desde youtube; esto siguiendo los pasos de instalación de:
"https://ytdl-org.github.io/youtube-dl/download.html". Se puede requerir "curl"
o un "chmod" específico es caso de dar problemas a la hora de la instalación.
- Se debe instalar el reproductor "mplayer" para poder reproducir el audio del video
desde la terminal; "sudo apt install mplayer".
- Para la compilación del programa se utiliza el comando "make" en la terminal y,
posterior a ello, se ejecuta con el comando "./Programa" seguido del parámetro
(link del video). Ej: "./Programa link_vidio_balong".
