#include <unistd.h>
#include <sys/wait.h>
#include <iostream>
#include <string.h>


using namespace std;


// Creación de la clase con la cual se descargará el audio del video.
class Descargar_audio{

    // Atributos privados.
    private:
        // Variable tipo string del link del video que se le entregará.
        string link_vidio;
        // Variable para la creación del proceso hijo.
        pid_t pid;

    // Atributos públicos.
    public:
        // Honor a Manoel (vidio = video).
        Descargar_audio(string link_vidio){
            // Asignacación a variable link_video.
            this->link_vidio = link_vidio;
            // Función para crear al proceso hijo con la función "fork".
            crear_proceso();
            // Función para descargar el audio en formato mp3.
            audio_mp3();
        }

        // Creación del proceso hijo.
        void crear_proceso(){
            pid = fork();
        }

        // Descarga del audio del video en mp3.
        void audio_mp3(){

            // Si el proceso hijo no se crea.
            if(pid < 0){
                // Se indica que existe un error.
                cout << "Error al crear el proceso hijo." << endl;
            }

            // Si el proceso hijo se crea.
            else if(pid == 0){
                // Se indica que el audio del video se descargará a través de su
                // link y se procede a descargar el mismo en formato mp3.
                cout << "\nSe descargaŕa el audio desde el siguiente link: '" << this->link_vidio;
                cout << "' En formato 'mp3'" << endl << endl;
                /* Utilización de youtube-dl para descargar el video, se especifica
                   que solo se quiere descargar el audio y el formato en particular (mp3).
                   Además, se realiza un cambio de nombre al archivo descargado para
                   poder utilizarlo en el proceso padre. */
                execlp("youtube-dl", "youtube-dl", "--extract-audio", "--audio-format", "mp3", this->link_vidio.c_str(), "-o", "audio_vidio.mp3", NULL);
            }

            // Se indica que el audio se ha descargado con éxito.
            else{
                wait(NULL);
                cout << "\nEl audio se ha descargado con éxito. \n" << endl;
            }
        }
};


// Función principal que solicita el parámetro para iniciar el programa (link).
int main(int argc, char *argv[]){

    // Se indica que se debe ingresar un parámetro.
    if(argc != 2){
        // Indicaciones de como se debe ingresar el párametro.
        cout << "\nPor favor, para iniciar el programa ingrese 1 parámetro." << endl;
        cout << "Este corresponde al link de youtube del cual quiere descargar " << endl;
        cout << "el audio y, posteriormente, reproducir." << endl << endl;
        cout << "Ejemplo de ejecución: './Programa youtube-link'" << endl << endl;
    }
    // Sino, se inicia el programa.
    else{
        // Se ejecuta la clase.
        Descargar_audio Descargar_audio(argv[1]);

        cout << "Se reproducirá el audio que se ha descargado." << endl << endl;

        // Se procede a reproducir el audio del video que se ha descargado con el
        // proceso hijo gracias a "mplayer", al cual se le entrega el nombre del
        // audio indicado en el proceso hijo.
        execlp("mplayer", "mplayer", "audio_vidio.mp3", NULL);
    }
    return 0;
}
