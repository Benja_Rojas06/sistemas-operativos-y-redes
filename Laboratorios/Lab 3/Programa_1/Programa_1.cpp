#include <iostream>
#include <fstream>
#include <string.h>
#include <time.h>


using namespace std;


// Función para calcular el tiempo de ejecución, en segundos, del programa.
float calc_tiempo(float tiempo_ini, float tiempo_fin){

    // Se calcula el tiempo de ejecución.
    float tiempo_ejec = (tiempo_fin - tiempo_ini)/CLOCKS_PER_SEC;

    // Se retorna el tiempo de ejecución.
    return tiempo_ejec;
}


// Función utilizada para contar las líneas del archivo.
int contar_lineas(ifstream &archivo){

    // Contador para las líneas.
    int cont_lineas = 0;

    // Almacenador de líneas.
    char linea[3000];

    // Recorre el archivo.
    while(!archivo.eof()){
        // Se detecta la línea dentro del archivo.
        archivo.getline(linea, 3000);
        // Se suma 1 línea al contador.
        cont_lineas++;
    }

    // Se indica la cantidad de líneas del archivo.
    cout << "    -> Cantidad de líneas: " << cont_lineas << endl;

    // Se retorna el total de líneas contadas para, posteriormente, sumarlas.
    return cont_lineas;
}


// Función utilizada para contar las palabras del archivo.
int contar_palabras(ifstream &archivo){

    // Contador para las palabras.
    // Se inicializa en "-1", ya que con esto se resta la línea en
    // blanco al final de los archivos.
    int cont_palabras = -1;

    // Almacenador de palabras.
    char palabra[300];

    // Recorre el archivo.
    while(!archivo.eof()){
        // Se detecta la palabra dentro del archivo.
        archivo>>palabra;
        // Se suma 1 palabra al contador.
        cont_palabras++;
    }

    // Se indica la cantidad de palabras del archivo.
    cout << "    -> Cantidad de palabras: " << cont_palabras << endl;

    // Se retorna el total de palabras contadas para, posteriormente, sumarlas.
    return cont_palabras;
}


// Función utilizada para contar los caracteres del archivo.
int contar_caracteres(ifstream &archivo){

    // Contador para los caracteres.
    int cont_caracteres = 0;

    // Almacenador de líneas para contar palabras.
    string linea;

    // Recorre el archivo.
    while(!archivo.eof()){
        // Se obtiene línea del archivo.
        getline(archivo, linea);

        // Variable numérica que representa el largo la línea.
        int num_carac = linea.length();

        // Se recorre la línea para contar los caracteres.
        for(unsigned int i = 0; i<linea.length(); i++){
            // Si se encuentra un caracter vacío (espacio).
            if(linea.at(i) == ' '){
                // Se resta uno.
                num_carac--;
            }
        }

        // Se suman los caracteres a nuestro contador.
        cont_caracteres = cont_caracteres + num_carac;
    }

    // Se indica la cantidad de palabras del archivo.
    cout << "    -> Cantidad de caracteres: " << cont_caracteres << endl;

    // Se retorna el total de palabras contadas para, posteriormente, sumarlas.
    return cont_caracteres;
}


// Función para realizar el reconocimiento de los archivos ingresados.
int main(int argc, char *argv[]){

    // Se indica que se deben ingresar los archivos de texto.
    if(argc <= 1){
        cout << "\nDebe ingresar archivos de texto para que comience el programa..." << endl;
    }

    // Almacena archivos de texto.
    ifstream archivo;

    // Variables para almacenar la suma de líneas, palabras y caracteres de los
    // archivos ingresados.
    int sum_lineas;
    int sum_palabras;
    int sum_caracteres;

    // Variables para realizar la toma de tiempo.
    float tiempo_ini, tiempo_fin;

    // Se toma el tiempo cuando inicia el programa.
    tiempo_ini = clock();

    // Ciclo para recorrer el/los archivos ingresados.
    for(int i=1; i<argc; i++){
        // Se indica el nombre de los archivos ingresados uno por uno.
        cout << "\n" << i << ".- El nombre del archivo ingresado es: " << argv[i] << endl;

        // Se abre el archivo para contar la cantidad de líneas.
        archivo.open(argv[i]);
        // Llamado función para contar las líneas del archivo y, además, se realiza
        // la suma necesaria para obtener el total de líneas.
        sum_lineas = sum_lineas + contar_lineas(archivo);
        // Se cierra el archivo una vez contadas las líneas.
        archivo.close();

        // Se abre el archivo para contar la cantidad de palabras.
        archivo.open(argv[i]);
        // Llamado función para contar las palabras del archivo y, además, se
        // realiza la suma necesaria para obtener el total de estas.
        sum_palabras = sum_palabras + contar_palabras(archivo);
        // Se cierra el archivo una vez contadas las palabras.
        archivo.close();

        // Se abre el archivo para contar la cantidad de caracteres.
        archivo.open(argv[i]);
        // Llamado función para contar las caracteres del archivo y, además, se
        // realiza la suma necesaria para obtener el total de estas.
        sum_caracteres = sum_caracteres + contar_caracteres(archivo);
        // Se cierra el archivo una vez contadas las caracteres.
        archivo.close();
    }

    // Sumas respectivas para cantidad de líneas, palabras y caracteres.
    cout << "\nResumen de las sumas solicitadas: " << endl << endl;
    cout << "-> Suma de líneas de los archivos: " << sum_lineas << endl;
    cout << "-> Suma de palabras de los archivos: " << sum_palabras << endl;
    cout << "-> Suma de caracteres de los archivos: " << sum_caracteres << endl;

    // Se toma el tiempo cuando finaliza el programa.
    tiempo_fin = clock();

    // Se indica el tiempo de ejecución llamando a la función para realizar el calculo.
    cout << "\nEl tiempo de ejecución es de '" << calc_tiempo(tiempo_ini, tiempo_fin) << "' segundos" << endl << endl;

    return 0;
}
