# Propuesta solución Laboratorio 3, Programa 1:

- Creado por: Benjamín Rojas Rojas.
- Docente: Alejandro Valdés Jiménez.
- Ayudante: Claudio Guevara.
- Módulo: Sistemas Operativos y Redes.


## Acerca del programa:

- Consiste en un programa que recibe una lista de archivos (de texto) y que cuenta
sus líneas, palabras y caracteres, gracias a funciones respectivas que recorren los
archivos. Además, se indica la suma de los parámetros mencionados de los archivos
ingresados; esto es, el total de líneas del archivo 1 se suman con el total de líneas
del archivo 2 y así sucesivamente con el resto de archivos.  
- Por último y, a modo de comparación con el programa 2, se realiza el cálculo del
tiempo de ejecución.

## Funcionamiento e Interacción:

- El programa se inicializa ingresando "n" cantidad de archivos (parámetros), luego
de ingresar el comando "./Programa" en la terminal, con el cual se ejecuta el mismo.
Un ejemplo es: "./Programa archivo1.txt archivo2.txt". De este modo se le indica al
programa que queremos realizar el conteo de líneas, palabras y caracteres de esos
archivos, además, de la suma de cada uno de estos entre los archivos ingresados.
Estos archivos se recorrerán uno por uno para calcular la cantidad de líneas
(con contar_lineas), la cantidad de caracteres (con contar_caracteres) y la cantidad
de palabras (con contar_palabras), luego se relizará un cálculo de la suma entre
estos archivos y, para finalizar, se indicará por la terminal los archivos ingresados
por el usuario con sus respectivos nombres y cantidad de líneas, caracteres y palabras;
la suma total de estos entre todos los archivos ingresados y el tiempo total, en
segundos, de ejecución del programa.
- Cabe mencionar que si no se ingresa ningún archivo, el programa indicará que existe
un error, dará un ejemplo de como se debe ingresar el archivo y se cerrará.


## Fallos que posee el programa:

- Si no se ingresa un archivo de texto, el programa se romperá y se deberá cerrar con
"ctrl + C".
- Hay veces en las que en la suma de las líneas aparece un número negativo (ocurre con
muy poca frecuencia).


## Construcción del programa:

- Sistema operativo: Ubuntu 18.04.4 LTS.
- Atom: Editor de texto utilizado para escribir el código del programa.
- C++: Lenguaje utilizado para resolver la problemática.


## Puntos a considerar:

- Para la suma de caracteres, considerar que el tilde se cuenta como otro caracter.
- Se debe realizar la instalación de "make" para que no ocurra ningún problema
en la compilación, esto con "sudo apt install make".
- Para la compilación del programa se utiliza el comando "make" en la terminal y,
posterior a ello, se ejecuta con el comando "./Programa" seguido del parámetro
(archivo de texto). Ej: "./Programa archivo1.txt".
