#include <iostream>
#include <pthread.h>
#include <fstream>
#include <string.h>
#include <time.h>


using namespace std;


// Función para calcular el tiempo de ejecución, en segundos, del programa.
float calc_tiempo(float tiempo_ini, float tiempo_fin){

    // Se calcula el tiempo de ejecución.
    float tiempo_ejec = (tiempo_fin - tiempo_ini)/CLOCKS_PER_SEC;

    // Se retorna el tiempo de ejecución.
    return tiempo_ejec;
}


// Creación de la estructura a utilizar en el programa.
struct program_structure{

    // Almacena archivos de texto.
    ifstream archivo;

    // Variables para realizar los conteos de las líneas, palabras y caracteres
    // para luego sumarlos.
    int cont_lineas;
    int cont_palabras;
    int cont_caracteres;
};


// Función utilizada para contar las líneas.
void *contar_lineas(void *param){

    program_structure *recibo_datos;

    recibo_datos = (program_structure *)param;

    // Contador para las líneas.
    int cont_lineas = 0;

    // Almacenador de líneas.
    char linea[3000];

    // Recorre el archivo.
    while(!recibo_datos->archivo.eof()){
        // Se detecta la línea dentro del archivo.
        recibo_datos->archivo.getline(linea, 3000);
        // Se suma 1 línea al contador.
        cont_lineas++;
    }

    // Se obtienen las líneas por archivo para poder sumarlas.
    recibo_datos->cont_lineas = cont_lineas;

    // Se indica la cantidad de líneas del archivo.
    cout << "    -> Cantidad de líneas: " << cont_lineas << endl;

    // Se elimina la hebra.
    pthread_exit(0);
}


// Función utilizada para contar las palabras del archivo.
void *contar_palabras(void *param){

    program_structure *recibo_datos;

    recibo_datos = (program_structure *)param;

    // Contador para las palabras.
    // Se inicializa en "-1", ya que con esto se resta la línea en
    // blanco al final de los archivos.
    int cont_palabras = -1;

    // Almacenador de palabras.
    char palabra[300];

    // Recorre el archivo.
    while(!recibo_datos->archivo.eof()){
        // Se detecta la palabra dentro del archivo.
        recibo_datos->archivo>>palabra;
        // Se suma 1 palabra al contador.
        cont_palabras++;
    }

    // Se obtienen las palabras por archivo para poder sumarlas.
    recibo_datos->cont_palabras = cont_palabras;

    // Se indica la cantidad de palabras del archivo.
    cout << "    -> Cantidad de palabras: " << cont_palabras << endl;

    // Se elimina la hebra.
    pthread_exit(0);
}

/*
// Función utilizada para contar los caracteres del archivo.
void *contar_caracteres(void *param){

    program_structure *recibo_datos;

    recibo_datos = (program_structure *)param;

    // Contador para los caracteres.
    int cont_caracteres = 0;

    // Almacenador de líneas para contar palabras.
    string linea;

    // Recorre el archivo.
    while(!recibo_datos->archivo.eof()){
        // Se obtiene línea del archivo.
        recibo_datos->getline(archivo, linea);

        // Variable numérica que representa el largo la línea.
        int num_carac = linea.length();

        // Se recorre la línea para contar los caracteres.
        for(unsigned int i = 0; i<linea.length(); i++){
            // Si se encuentra un caracter vacío (espacio).
            if(linea.at(i) == ' '){
                // Se resta uno.
                num_carac--;
            }
        }

        // Se suman los caracteres a nuestro contador.
        cont_caracteres = cont_caracteres + num_carac;
    }

    // Se indica la cantidad de palabras del archivo.
    cout << "    -> Cantidad de caracteres: " << cont_caracteres << endl;

    // Se elimina la hebra.
    pthread_exit(0);
}
*/

// Función principal que solicita el parámetro para iniciar el programa (link).
int main(int argc, char *argv[]){

    // Se indica que se deben ingresar los archivos de texto.
    if(argc <= 1){
        cout << "\nDebe ingresar archivos de texto para que comience el programa..." << endl;
        cout << "Ej: ./Programa archivo1.txt archivo2.txt archivo3.txt" << endl;
    }

    // Creación de las hebras, se multiplica por 3, ya que se deben crear 3 hebras
    // por archivo.
    pthread_t threads[argc*3-1];

    // Creación variable "envio_datos" que contiene el archivo.
    program_structure envio_datos;

    // Variables para almacenar la suma de líneas, palabras y caracteres de los
    // archivos ingresados.
    int sum_lineas;
    int sum_palabras;
    int sum_caracteres;

    // Variables para realizar la toma de tiempo.
    float tiempo_ini, tiempo_fin;

    // Se toma el tiempo cuando inicia el programa.
    tiempo_ini = clock();

    // Ciclo para recorrer el/los archivos ingresados.
    for(int i=1; i<argc; i++){
        // Se indica el nombre de los archivos ingresados uno por uno.
        cout << "\n" << i << ".- El nombre del archivo ingresado es: " << argv[i] << endl;

        // Se llama a la estructura que contiene al archivo para luego abrirlo.
        envio_datos.archivo.open(argv[i]);
        // Creación de la hebra para contar las líneas.
        pthread_create(&threads[i-1], NULL, contar_lineas, (void *)&envio_datos);
        // Se espera hasta que se creen todas las hebras.
        pthread_join(threads[i-1], NULL);
        // Se llama a la estructura que contiene al archivo para luego cerrarlo.
        envio_datos.archivo.close();
        // Suma para obetener el total de las líneas de los archivos ingresados.
        sum_lineas = sum_lineas + envio_datos.cont_lineas;

        // Se llama a la estructura que contiene al archivo para luego abrirlo.
        envio_datos.archivo.open(argv[i]);
        // Creación de la hebra para contar las palabras.
        pthread_create(&threads[i-1], NULL, contar_palabras, (void *)&envio_datos);
        // Se espera hasta que se creen todas las hebras.
        pthread_join(threads[i-1], NULL);
        // Se llama a la estructura que contiene al archivo para luego cerrarlo.
        envio_datos.archivo.close();
        // Suma para obetener el total de las palabras de los archivos ingresados.
        sum_palabras = sum_palabras + envio_datos.cont_palabras;

        /*
        // Se llama a la estructura que contiene al archivo para luego abrirlo.
        envio_datos.archivo.open(argv[i]);
        // Creación de la hebra para contar los caracteres.
        pthread_create(&threads[i-1], NULL, contar_caracteres, (void *)&envio_datos);
        // Se espera hasta que se creen todas las hebras.
        pthread_join(threads[i-1], NULL);
        // Se llama a la estructura que contiene al archivo para luego cerrarlo.
        envio_datos.archivo.close();
        // Suma para obetener el total de los caracteres de los archivos ingresados.
        sum_caracteres = sum_caracteres + envio_datos.cont_caracteres;
        */
    }

    // Sumas respectivas para cantidad de líneas, palabras y caracteres.
    cout << "\nResumen de las sumas solicitadas: " << endl << endl;
    cout << "-> Suma de líneas de los archivos: " << sum_lineas << endl;
    cout << "-> Suma de palabras de los archivos: " << sum_palabras << endl;
    cout << "-> Suma de caracteres de los archivos: " << sum_caracteres << endl;

    // Se toma el tiempo cuando finaliza el programa.
    tiempo_fin = clock();

    // Se indica el tiempo de ejecución llamando a la función para realizar el calculo.
    cout << "\nEl tiempo de ejecución es de '" << calc_tiempo(tiempo_ini, tiempo_fin) << "' segundos" << endl << endl;

    return 0;
}
